import Vapor
import FluentPostgreSQL

final class Category: Codable {
    var id: Int?
    var name: String
    
    init(name: String) {
        self.name = name
    }
}

extension Category {
    var acronyms: Siblings<Category, Acronym, AcronymCategoryPivot> {
        return siblings()
    }
    
    static func addCategory(_ name: String, to acronym: Acronym, on req: Request) -> Future<Void> {
        return Category.query(on: req).filter(\.name == name).first().flatMap { category in
            if let category = category {
                return acronym.categories.attach(category, on: req).transform(to: ())
            } else {
                let category = Category(name: name)
                return category.save(on: req).flatMap { category in
                    return acronym.categories.attach(category, on: req).transform(to: ())
                }
            }
        }
    }
}

extension Category: PostgreSQLModel {}

extension Category: Content {}

extension Category: Migration {}

extension Category: Parameter {}
